package DAO;

import Classes.Aluno;
import Classes.Telefone;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DAO {

    public void cadastrar(Aluno aluno, Telefone telefone) throws SQLException{
    
        PreparedStatement prepstat;
        
        String a= "insert into Aluno(nome, idade, cpf, rg)"
                + "values(?,?,?,?)";
        
        String b="insert into Telefone(numero, operadora, ddd)"
                + "values(?,?,?)";
        
        prepstat= Connection.ConnectionFactory.connectionPaulo().prepareStatement(a);
        prepstat.setString(1, aluno.getNome());
        prepstat.setInt(2, aluno.getIdade());
        prepstat.setString(3, aluno.getCpf());
        prepstat.setString(4, aluno.getRg());
        
        prepstat.executeUpdate();
        
        prepstat= Connection.ConnectionFactory.connectionPaulo().prepareStatement(b);
        prepstat.setString(1, telefone.getNumero());
        prepstat.setString(2, telefone.getOperadora());
        prepstat.setString(3, telefone.getDdd());
        
        prepstat.executeUpdate();
        
    }
    
}
