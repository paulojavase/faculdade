package Connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnectionFactory {

    private static final String DRIVER= "com.mysql.cj.jdbc.Driver";
    private static final String URL= "jdbc:mysql://localhost:3306/faculdade?useTimezone=true&serverTimezone=UTC";
    private static final String USER= "root";
    private static final String PASS= "root";
    
    static Connection con;
    
    public static Connection connectionPaulo() {
        
        try {
            Class.forName(DRIVER);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ConnectionFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            con= DriverManager.getConnection(URL, USER, PASS);
        } catch (SQLException ex) {
            Logger.getLogger(ConnectionFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return con;
        
        
       
    }

    public static void main(String[] args) throws ClassNotFoundException {
        
        ConnectionFactory cnf= new ConnectionFactory();
        cnf.connectionPaulo();
    }
}